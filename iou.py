import pandas as pd
import numpy as np
from pdf2image import convert_from_bytes, convert_from_path
import copy
from thefuzz import fuzz
from thefuzz import process

#function for calculating IoU
def bb_intersection_over_union(bb1, bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------
    bb1 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x1, y1) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner
    bb2 : dict
        Keys: {'x1', 'x2', 'y1', 'y2'}
        The (x, y) position is at the top left corner,
        the (x2, y2) position is at the bottom right corner

    Returns
    -------
    float
    in [0, 1]
    """
    #box_1 = [511, 41, 577, 76]
    #box_2 = [544, 45, 570, 50]
    assert bb1['x1'] < bb1['x2']
    assert bb1['y1'] < bb1['y2']
    assert bb2['x1'] < bb2['x2']
    assert bb2['y1'] < bb2['y2']
        

    # determine the coordinates of the intersection rectangle
    x_left = max(bb1['x1'], bb2['x1'])
    y_top = max(bb1['y1'], bb2['y1'])
    x_right = min(bb1['x2'], bb2['x2'])
    y_bottom = min(bb1['y2'], bb2['y2'])

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
    bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def find_IOU(ref_field_values, ref_fields_bbox_values, ocr_result):
    # list_of_refs = [ref for ref,bb in ref_fields_bbox_values]
    field_value_list=[]
    # print(len(ref_field_values), len(list_of_refs))

    for i in range(len(ref_field_values)):
        iou=[]
        # print(virtual_bbox_values[i][1])
        tl_main, tr_main, br_main, bl_main = ref_fields_bbox_values[i][1]
        tl_main = (int(tl_main[0]), int(tl_main[1]))
        tr_main = (int(tr_main[0]), int(tr_main[1]))
        br_main = (int(br_main[0]), int(br_main[1]))
        bl_main = (int(bl_main[0]), int(bl_main[1]))
        box_1 = {'x1':int(tl_main[0]), 'x2': int(br_main[0]), 'y1':int(tr_main[1]), 'y2':int(br_main[1])}

        for (bbox, text, prob) in ocr_result:
            
            # text = cleanup_text(text)
            tl, tr, br, bl = bbox
            tl = (int(tl[0]), int(tl[1]))
            tr = (int(tr[0]), int(tr[1]))
            br = (int(br[0]), int(br[1]))
            bl = (int(bl[0]), int(bl[1]))

            box_2 = {'x1':int(tl[0]), 'y1': int(tl[1]), 'x2':int(br[0]), 'y2':int(br[1])}
            
            # compute the intersection over union and display it
            iou.append(bb_intersection_over_union(box_1, box_2))

        iou_idx = []
        for io in iou:
            if io > 0.002:  
                 iou_idx.append(iou.index(io))
        
        field_result = []
        for idx in iou_idx:
            field_result.append(ocr_result[idx][1])

        ratio = []
        for ref in ref_field_values:
            # print(ref[0], ref_field_values[i][0])
            ratio.append(fuzz.ratio(ref[0], ref_field_values[i][0]))
        
        # print(ref[0],ratio)
        # (refs[ratio.index(max(ratio))][0], "".join(field_result))
        if ref_field_values[ratio.index(max(ratio))][1] == '':
            field_value=(ref_field_values[ratio.index(max(ratio))][0], " ".join(field_result)) #,'iou':iou[idx], 'maxIoU':max_iou}
        else:
            field_value=(ref_field_values[ratio.index(max(ratio))][0], ref_field_values[ratio.index(max(ratio))][1]+" ".join(field_result))
        # cv2.rectangle(image, (int(max_iou_tl[0]), int(max_iou_tl[1])), (int(max_iou_br[0]),int(max_iou_br[1])),
        # (255, 0, 50), 3)
        # plot_image(image, cmap=None)
        field_value_list.append(field_value)
    return field_value_list