# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
import cv2, copy
# from boxdetect import config
# from boxdetect.pipelines import get_boxes


# def get_cbs_ref(cbs_ref_file, formtype):
#     cbs_refs = pd.read_csv(cbs_ref_file)
#     cbs_refs = cbs_refs[cbs_refs['Form'] == formtype].reset_index()
#     return cbs_refs

def x_bb(bb):
    return bb[0]

def y_bb(bb):
    return bb[1]

def sort_bbs(bbs):
    y_sorted_bbs = sorted(bbs, key=y_bb)
    cell_bbs = []
    for y in y_sorted_bbs:
        if y not in cell_bbs:
            row = []
            for bb in bbs:
                if y[1] == bb[1]:
                    row.append(bb)
            row = sorted(row, key=x_bb)
            for cell in row:
                cell_bbs.append(cell)
    return cell_bbs


def get_table_details(tbl_im, ocr_reader):
    image=copy.deepcopy(tbl_im[1860:3330, 240:4010])
    gray_scale=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    img_height, img_width = gray_scale.shape[0], gray_scale.shape[1]

    img_bin = cv2.adaptiveThreshold(gray_scale, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 21, 5)

    kernel_v = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 30))
    kernel_h = cv2.getStructuringElement(cv2.MORPH_RECT, (30, 1))

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))

    image_1 = cv2.erode(img_bin, kernel_v, iterations=1)
    vertical_lines = cv2.dilate(image_1, kernel_v, iterations=3)


    image_2 = cv2.erode(img_bin, kernel_h, iterations=1)
    horizontal_lines = cv2.dilate(image_2, kernel_h, iterations=3)


    img_vh = cv2.addWeighted(vertical_lines, 1, horizontal_lines, 1, 0)
    img_vh = cv2.erode(~img_vh, kernel, iterations=2)
    thresh, img_vh = cv2.threshold(img_vh, 128, 255, cv2.THRESH_BINARY)
    # plot_image(img_vh, cmap='gray')

    contours, hierarchy = cv2.findContours(img_vh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    boundingbxs = [cv2.boundingRect(c) for c in contours]
    tblcells = sort_bbs(boundingbxs)

    tblcells_text = []
    i = 1
    for x,y,w,h in tblcells:
        tblrow = {}
        if (0.1*img_width < w < 0.6*img_width and h > 0.09*img_height):
            text = ocr_reader.readtext(image[y:y+h, x:x+w])
            if text:
                if i == 1:
                    # print(f"Name - {text[0][1]}")
                    tblrow['Name'] = text[0][1]
                    i+=1
                elif i == 2:
                    # print(f"Address - {text[0][1]}")
                    tblrow['Address'] = text[0][1]
                    i+=1
                elif i == 3:
                    # print(f"TIN - {text[0][1]}")
                    tblrow['TIN'] = text[0][1]
                    i = 1
                # plot_image(image[y:y+h, x:x+w], cmap=None)
            # image = cv2.rectangle(image, (x, y), (x + w, y + h), (200, 110, 100), 5)
            # image
            # plot_image(image[y:y+h, x:x+w], cmap=None)
            if tblrow != {}:
                tblcells_text.append(tblrow)
    return tblcells_text
