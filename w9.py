import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2
from boxdetect import config
from boxdetect.pipelines import get_boxes


def get_cbs_ref(cbs_ref_file, formtype):
    cbs_refs = pd.read_csv(cbs_ref_file)
    cbs_refs = cbs_refs[cbs_refs['Form'] == formtype].reset_index()
    return cbs_refs

def x_cb(e):
    return e[0]

def y_cb(e):
    return e[1]

def sort_cbs_bb(cbs):
    y_sort_cbs = sorted(cbs, key=y_cb)

    cbs_rows = []
    i = 0
    while i < len(y_sort_cbs):
        row = [y_sort_cbs[i]]
        for j in range(i+1,len(y_sort_cbs)):
            # temp = i
            if y_sort_cbs[i][1]> y_sort_cbs[j][1]-y_sort_cbs[j][2]*0.25 and y_sort_cbs[i][1]< y_sort_cbs[j][1]+y_sort_cbs[j][2]*0.25:
                row.append(y_sort_cbs[j])
                temp = j
        if len(row) > 1:
            i = temp+1
            temp = i
        else:
            i +=1
        cbs_rows.append(row)
        
    # print(cbs_rows)
    cbs_sorted = []

    for row in cbs_rows:
        row_sort = sorted(row,key=x_cb)
        for item in row_sort:
            cbs_sorted.append(item)

    return cbs_sorted
    

def get_checked_cbs_pos(cbs, checked_cbs):
    cbs_matched = []
    for i in range(len(cbs)):
        for j in range(len(checked_cbs)):
            if ''.join([str(cbs[i][0]),str(cbs[i][1])]) == ''.join([str(checked_cbs[j][0]),str(checked_cbs[j][1])]):
                # print(''.join(str(elem) for elem in bxs[i]), ''.join(str(elem) for elem in cbs[j]))
                # cv2.rectangle(im, (cbs[j][0], cbs[j][1]),(cbs[j][0]+cbs[j][2], cbs[j][1]+cbs[j][2]), (255,100,100),5)            
                cbs_matched.append(i)
    return cbs_matched

def get_w9_tin_nums(file_name, ocr_reader):
    image = cv2.imread(filename=file_name)
    cfg = config.PipelinesConfig()

    im_width, im_height = image.shape[1], image.shape[0]
    # w_min, w_max = 0.022*im_width, 0.024*im_width
    w_min , w_max = 95,100
    # h_min, h_max = 0.03*im_height, 0.032*im_height
    h_min , h_max = 165,175
    cfg.width_range = (w_min, w_max)
    cfg.height_range = (h_min, h_max)
    cfg.scaling_factors = [1]
    cfg.wh_ratio_range = (0.5, 1.5)
    cfg.group_size_range = (2,2)
    cfg.dilation_iterations = 0

    ssin, ein = "", ""
    bxs, grouping_rects, image, output_image = get_boxes(file_name, cfg=cfg, plot=False)
    # plot_image(output_image, cmap=None)
    # convert each bb from 4,0 numpy array to a list
    if bxs != ():
        bxs = [list(bxs[i]) for i in range(bxs.shape[0])]
        bxs = sort_cbs_bb(bxs)
        # print(len(bxs))
        image = cv2.imread(filename=file_name)
        tin = ''
        for bb in bxs:
            x,y,w,h = bb
            xmin, xmax = y, y+h
            ymin, ymax = x, x+w
            # cv2.rectangle(image, (x,y), (x+w,y+h), (255, 0, 0), 2)
            # plot_image(image[xmin:xmax, ymin:ymax], cmap=None)
            res = ocr_reader.readtext(image[xmin:xmax, ymin:ymax])
            if res != []:
                tin +=  res[0][1]
            else:
                tin += ' '
        
        ssin, ein = tin[:9], tin[9:]
        # print(ssin, ein)
    return ssin, ein

