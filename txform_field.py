import pandas as pd
import numpy as np
from pdf2image import convert_from_bytes, convert_from_path
import copy
from thefuzz import fuzz
from thefuzz import process

import matplotlib.pyplot as plt
import cv2

from iou import find_IOU



def cleanup_text(text):
	# strip out non-ASCII text so we can draw the text on the image using OpenCV
	# return "".join([c if ord(c) < 128 else "" for c in text]).strip().lower()
	return "".join([ch for ch in text if (ord(ch) in range(ord('a'),ord('z')+1,1) )
	 or (ord(ch) in range(ord('A'),ord('Z')+1,1)) or ord(ch) == ord(" ")]).strip()

def get_form_ocr_results(ocr_reader, imgs, form_fields_ref):
    # print("\n********* Running OCR ***********")
    ocr_result = ocr_reader.readtext(imgs)
    ocr_result_copy = copy.deepcopy(ocr_result)
    ocr_result_matches, ref_field_values = get_fuzzy_matches(form_fields_ref,ocr_result_copy)
    # print(ocr_result_matches)
    return ocr_result, ocr_result_matches, ref_field_values


def get_fuzzy_matches(form_fields_ref, results):

    fuzzy_matches_list, ref_field_values = [], []
    for ref in list(form_fields_ref['Field']):
         ref_field_values.append((ref,''))

    for i in range(len(form_fields_ref)):
        right, left = False, False
        form_field_pos = form_fields_ref.loc[i,'Position'].split(';')

        for pos in form_field_pos:
            if pos == 'right':
                right = True

        print(f"\n**** Getting Fuzzy match for '{form_fields_ref.loc[i,'Field']}' ****")
        ratio = [0]*len(results)
        key_and_bb = {}
        for j in range(len(results)):
            text = cleanup_text(results[j][1])
            ratio[j] = fuzz.ratio(cleanup_text(form_fields_ref.loc[i,'Field anchor']).lower(), text.lower())

        # ratio = [r for i, r in enumerate(ratio) if r > 90] 
        max_ratio = [i for i, r in enumerate(ratio) if r == max(ratio) and r>50]
        # print(max_ratio)
        if len(max_ratio) > 0:

            if right == True:
                field_ref = form_fields_ref.loc[i,'Field anchor']
                max_ratio_text = results[max_ratio[0]][1]
                if len(max_ratio_text[max_ratio_text.find(field_ref[-4:])+4:])>0:
                    # print(max_ratio_text[max_ratio_text.find(field_ref[-4:])+4:])
                    ref_field_values[i] = ((form_fields_ref.loc[i,'Field'],max_ratio_text[max_ratio_text.find(field_ref[-4:])+4:]))

            key_and_bb = {'field': form_fields_ref.loc[i,'Field'], 'text_in_the_document': results[max_ratio[0]][1],
                            'bb': results[max_ratio[0]][0]} #, 'max raio': max_ratio[i], 'ratio': ratio[i]}
            fuzzy_matches_list.append(key_and_bb)
            tl, tr, br, bl = results[max_ratio[0]][0]
            tl = (int(tl[0]), int(tl[1]))
            tr = (int(tr[0]), int(tr[1]))
            br = (int(br[0]), int(br[1]))
            bl = (int(bl[0]), int(bl[1]))
            print(f"found - {results[max_ratio[0]][1]}")
            results.pop(max_ratio[0])
        else:
            key_and_bb = {'field': form_fields_ref.loc[i,'Field'], 'text_in_the_document': None,'bb': None} #, 'max raio': max_ratio[i], 'ratio': ratio[i]}
            fuzzy_matches_list.append(key_and_bb)
            ref_field_values[i]=((form_fields_ref.loc[i,'Field'],''))
            print(f"not found")
    return fuzzy_matches_list,ref_field_values
    

# def get_ref_fields_bbs(image, form_refs,ocr_results):
#     #Below codes create the virtual bounding boxes for each interested fields
#     line_width=0
#     virtual_bbox_values=[]
#     im_h, im_w = image.shape[0], image.shape[1]
#     for row in range(len(form_refs)):
#         form_refs_pos = form_refs.loc[row, "Position"].split(';')
#         form_refs_size = str(form_refs.loc[row, "SizeFactor"]).split(';')
#         form_refs_factor = str(form_refs.loc[row, "Factor"]).split(';')
#         # print(form_refs.loc[row, "Field"])
        
#         for pos,size,fc in zip(form_refs_pos, form_refs_size, form_refs_factor):
#                 size, fc = float(size), float(fc)

#                 if ocr_results[row]['bb'] != None:
#                     tl_main, tr_main, br_main, bl_main = ocr_results[row]['bb']
#                     if(pos=='down'):
#                             tl_main = (int(bl_main[0]), int(bl_main[1])+15)
#                             tr_main = (int(br_main[0]*fc), int(br_main[1])+15)
#                             br_main = (int(br_main[0]*fc), int(br_main[1]+im_h*size)-15)
#                             bl_main = (int(bl_main[0]), int(bl_main[1]+im_h*size)-15)
#                             virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl_main, tr_main, br_main, bl_main]))
#                         #     cv2.rectangle(image, tl_main, br_main, (100,0 , 200), 5)
#                             #print(virtual_bbox_values[item])
#                     elif(pos=='right'):
#                             tl_main = (int(tr_main[0]), int(tr_main[1])+15)
#                             tr_main = (int(tr_main[0]+im_w*size*fc), int(tr_main[1])+15)
#                             br_main = (int(br_main[0]+im_w*size*fc), int(br_main[1])-15)
#                             bl_main = (int(br_main[0]), int(br_main[1])-15)
#                             virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl_main, tr_main, br_main, bl_main]))
#                         #     cv2.rectangle(image, tl_main, br_main, (100,0 , 200), 5)
#                             #print(virtual_bbox_values[item])
#                     elif(pos=='left'):
#                             # cv2.rectangle(image, tl_main, br_main, (100,0 , 200), 5)
#                             tl_main = (int(tl_main[0]-im_w*size*fc), int(tr_main[1]))
#                             tr_main = (int(tl_main[0]), int(tl_main[1]))
#                             br_main = (int(bl_main[0]), int(bl_main[1]))
#                             bl_main = (int(tr_main[0]-im_w*size*fc), int(br_main[1]))
#                             virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl_main, tr_main, br_main, bl_main]))
#                         #     cv2.rectangle(image, tl_main, br_main, (100,0 , 200), 5)
#                             #print(virtual_bbox_values[item])
#                     elif(pos=='up'):
#                             # cv2.rectangle(image, tl_main, br_main, (100,0 , 200), 5)
#                             br_main = (int(tr_main[0]*fc), int(tr_main[1]))
#                             bl_main = (int(tl_main[0]), int(tl_main[1]))
#                             tl_main = (int(tl_main[0]), int(tl_main[1]-im_h*size))
#                             tr_main = (int(tr_main[0]*fc), int(tr_main[1]-im_h*size))
#                             virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl_main, tr_main, br_main, bl_main]))
#                         #     cv2.rectangle(image, tl_main, br_main, (100,0,200), 5)
#                             #print(virtual_bbox_values[item])
#                 else:
#                     virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],None))  
# #     plot_image(image, cmap=None)
#     return virtual_bbox_values

def get_ref_fields_bbs(image, form_refs,ocr_results):
    #Below codes create the virtual bounding boxes for each interested fields
    virtual_bbox_values=[]
    im_h, im_w = image.shape[0], image.shape[1]

    for row in range(len(form_refs)):
        ref_field_pos = form_refs.loc[row, "Position"].split(';')
        ref_field_h_fc = str(form_refs.loc[row, "height factor"]).split(';')
        ref_field_w_fc = str(form_refs.loc[row, "width factor"]).split(';')
        # print(form_refs.loc[row, "Field"])
        
        for pos,h_fc,w_fc in zip(ref_field_pos, ref_field_h_fc, ref_field_w_fc):
                h_fc,w_fc = float(h_fc), float(w_fc)

                if ocr_results[row]['bb'] != None:
                        tl_main, tr_main, br_main, bl_main = ocr_results[row]['bb']
                        bb_h = int(br_main[1]) - int(tl_main[1])
                        bb_w = int(tr_main[0]) - int(tl_main[0])

                        if(pos=='down'):
                                tl = (int(bl_main[0]), int(bl_main[1])+int(bb_h*0.2))
                                tr = (int(bl_main[0]) + int(bb_w*w_fc), int(br_main[1])+int(bb_h*0.2))
                                bl = (int(bl_main[0]), int(bl_main[1]) + int(bb_h*0.75*h_fc))
                                br = (int(bl_main[0] + int(bb_w*w_fc)), int(br_main[1])+int(bb_h*0.75*h_fc))
                                
                                
                                virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl, tr, br, bl]))
                                # cv2.rectangle(image, tl, br, (100,0 , 200), 5)

                        elif(pos=='right'):
                                tl = (int(tr_main[0]) + int(0.005*im_w), int(tr_main[1]) + int(bb_h*0.2))
                                tr = (int(tr_main[0]) + int(bb_w*w_fc), int(tr_main[1]) + int(bb_h*0.2))
                                bl = (int(br_main[0]) + int(0.005*im_w), int(tr_main[1]) + int(bb_h*0.75))
                                br = (int(br_main[0]) + int(bb_w*w_fc), int(tr_main[1]) + int(bb_h*0.75))

                                virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl, tr, br, bl]))
                                # cv2.rectangle(image, tl, br, (100,0 , 200), 5)

                        elif(pos=='left'):
                                tl = (int(tl_main[0]) - int(bb_w*w_fc), int(tr_main[1]) + int(bb_h*0.2))
                                tr = (int(tl_main[0]), int(tl_main[1]) + int(bb_h*0.2))
                                bl = (int(bl_main[0]) - int(bb_w*w_fc), int(bl_main[1]) + int(bb_h*0.75))
                                br = (int(bl_main[0]), int(bl_main[1]) + int(bb_h*0.75))
                                virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl, tr, br, bl]))
                                # cv2.rectangle(image, tl, br, (100,0 , 200), 5)

                        elif(pos=='up'):
                                tl = (int(tl_main[0]), int(tl_main[1]) - int(bb_h*0.2))
                                tr = (int(tl_main[0]) + int(bb_w*w_fc), int(tr_main[1])-int(bb_h*0.2))
                                bl = (int(tl_main[0]), int(tl_main[1]) - int(bb_h*0.75*h_fc))
                                br = (int(tl_main[0] + int(bb_w*w_fc)), int(tl_main[1])- int(bb_h*0.75*h_fc))
                                virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],[tl_main, tr_main, br_main, bl_main]))
                                # cv2.rectangle(image, tl, br, (100,0,200), 5)
                else:
                    virtual_bbox_values.append((ocr_results[row]['text_in_the_document'],None))  
    # plot_image(image, cmap=None)
    return virtual_bbox_values


def get_taxform_fields(pdf_images, form_refs, page_nos, ocr_reader):

    pdf_images_ocr = [get_form_ocr_results(ocr_reader, pdf_images[p], 
    form_refs[form_refs['PageNumber'] == p+1].reset_index()) 
    for p,im in zip(page_nos,pdf_images)]

    pdf_images_fields_bbs = [get_ref_fields_bbs(pdf_images[pg], 
    form_refs[form_refs['PageNumber'] == pg+1].reset_index(), pdf_images_ocr[i][1]) 
    for pg,i in zip(page_nos,range(len(pdf_images_ocr)))]

    pdf_images_bb_fields_values = []
    for pg,i in zip(page_nos,range(len(pdf_images_ocr))):
        image_fields_values = find_IOU(pdf_images_ocr[i][2],pdf_images_fields_bbs[i], pdf_images_ocr[i][0])
        pdf_images_bb_fields_values.append(image_fields_values)
    return pdf_images_bb_fields_values
