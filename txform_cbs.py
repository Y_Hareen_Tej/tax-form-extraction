import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import cv2
from boxdetect import config
from boxdetect.pipelines import get_boxes,get_checkboxes


def get_cbs_ref(cbs_ref_file, formtype):
    cbs_refs = pd.read_csv(cbs_ref_file)
    cbs_refs = cbs_refs[cbs_refs['Form'] == formtype].reset_index()
    return cbs_refs

def x_cb(e):
    return e[0]

def y_cb(e):
    return e[1]

def sort_cbs_bb(cbs):
    y_sort_cbs = sorted(cbs, key=y_cb)

    cbs_rows = []
    i = 0
    while i < len(y_sort_cbs):
        row = [y_sort_cbs[i]]
        for j in range(i+1,len(y_sort_cbs)):
            # temp = i
            if y_sort_cbs[i][1]> y_sort_cbs[j][1]-y_sort_cbs[j][2]*0.25 and y_sort_cbs[i][1]< y_sort_cbs[j][1]+y_sort_cbs[j][2]*0.25:
                row.append(y_sort_cbs[j])
                temp = j
        if len(row) > 1:
            i = temp+1
            temp = i
        else:
            i +=1
        cbs_rows.append(row)
        
    # print(cbs_rows)
    cbs_sorted = []

    for row in cbs_rows:
        row_sort = sorted(row,key=x_cb)
        for item in row_sort:
            cbs_sorted.append(item)

    return cbs_sorted
    

def get_checked_cbs_pos(cbs, checked_cbs):
    cbs_matched = []
    for i in range(len(cbs)):
        for j in range(len(checked_cbs)):
            if ''.join([str(cbs[i][0]),str(cbs[i][1])]) == ''.join([str(checked_cbs[j][0]),str(checked_cbs[j][1])]):
                # print(''.join(str(elem) for elem in bxs[i]), ''.join(str(elem) for elem in cbs[j]))
                # cv2.rectangle(im, (cbs[j][0], cbs[j][1]),(cbs[j][0]+cbs[j][2], cbs[j][1]+cbs[j][2]), (255,100,100),5)            
                cbs_matched.append(i)
    return cbs_matched


def get_img_cbs(file_name,cbs_ref):
    # image = cv2.imread(filename=file_name)
    cfg = config.PipelinesConfig()

    w_min, w_max=40,60
    h_min, h_max=40,60
    cfg.width_range = (w_min, w_max)
    cfg.height_range = (h_min, h_max)
    cfg.scaling_factors = [1]
    cfg.wh_ratio_range = (0.8, 1.2)
    cfg.group_size_range = (1,1)
    cfg.dilation_iterations = 0

    cbs, grouping_rects, image, output_image = get_boxes(file_name, cfg=cfg, plot=False)
    # plot_image(output_image, cmap=None)
    # convert each bb from 4,0 numpy array to a list
    cbs = [list(cbs[i]) for i in range(cbs.shape[0])]
    cbs = sort_cbs_bb(cbs)
    
    checked_cbs = get_checkboxes(file_name, cfg=cfg, px_threshold=0.1, plot=False, verbose=False)
    checked_cbs_bb = [checked_cbs[i][0] for i in range(checked_cbs.shape[0]) if checked_cbs[i][1]]
    cbs_ref.loc[:,'checked'] = 'no'
    checked_cbs_pos = get_checked_cbs_pos(cbs, checked_cbs_bb)
    
    cbs_ref.loc[checked_cbs_pos,'checked'] = 'yes'
    return cbs_ref             

def get_cbs_ref(cbs_ref_file, formtype):
    cbs_refs = pd.read_csv(cbs_ref_file)
    cbs_refs = cbs_refs[cbs_refs['Template'] == formtype].reset_index()
    return cbs_refs