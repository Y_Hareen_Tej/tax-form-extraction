import argparse

import easyocr
from flask import Flask, flash, request, redirect, url_for, render_template, jsonify
from werkzeug.utils import secure_filename
from taxformextraction import extract_tax_form

app = Flask(__name__)
app.secret_key = "qwerty"
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

reader = easyocr.Reader(['en'], gpu=False)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload-tax-form', methods=['GET','POST'])
def upload_tax_form():
    if request.method == 'GET':
        return render_template('upload.html')
    if request.method == 'POST':
        # check if the post request has the file part
        if 'fileToUpload' not in request.files:
            flash('No file uploaded')
            return redirect(request.host_url+'/upload-tax-form')

        file = request.files['fileToUpload']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # file_upload_path = os.path.join('.\\forms uploaded', filename)
            # file.save(file_upload_path)
            # form_type = ''
            # print(file.filename, request.form.getlist('formtype')[0])
            tax_form_fields = extract_tax_form(file.read(), request.form.getlist('formtype')[0],reader)
            # d = {'fields':fields, 'checkboxes':cbs, 'SSIN':ssin}
        return render_template('upload.html') #, jsonfile = jsonify(d))

if __name__ == '__main__':

    argparser = argparse.ArgumentParser()
    
    # argparser.add_argument("-i", "--pdf", required=True,
    #     help="path to input image to be OCR'd")
    # argparser.add_argument("-l", "--langs", type=str, default="en",
    #     help="comma separated list of languages to OCR")
    argparser.add_argument("-g", "--gpu", type=int, default=-1,
        help="whether or not GPU should be used")
    # argparser.add_argument("-f", "--form", type=str, default=-1,
    #     help="form that is being used")
    
    args = vars(argparser.parse_args())

    app.run(debug=True)


