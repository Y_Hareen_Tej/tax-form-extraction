import pandas as pd
import numpy as np
from pdf2image import convert_from_bytes
from fuzzywuzzy import fuzz
from txform_field import get_taxform_fields
from txform_cbs import get_cbs_ref, get_img_cbs
import cv2
from w9 import get_w9_tin_nums
from w8bene import get_table_details
# import fitz
# from PyPDF4 import PdfFileReader

def pdf_to_images(pdf):
    pages = convert_from_bytes(
        pdf, 500)
        # , poppler_path='C:\\Users\\yhareen.tej\\OneDrive - Zycus\\Documents\\POCs\\isupplier\\tax form extraction\\app\\poppler-0.68.0\\bin')
    return [np.array(p) for p in pages]

def read_pdf(pdf_path):
    pdfFileObject = open(pdf_path, 'rb')
    pdf_reader = PdfFileReader(pdfFileObject)
    information = pdf_reader.getDocumentInfo()
    return pdf_reader,information

def get_form_ref(form_refs, formtype):
    form_refs = pd.read_csv(form_refs)
    return form_refs[form_refs['Template'] == formtype].reset_index()

def extract_tax_form(pdf, formtype, ocr_reader):
    ## below code should be enabled for pure editable pdfs to extract fields, cbs, tbls
    # is_pdf_structured = False
    # pdfFileObject = open(pdf, 'rb')
    # pdf_reader = PdfFileReader(pdfFileObject)
    # pdf_information = pdf_reader.getDocumentInfo()
    # if '/Accessibility' in pdf_information.keys():
    #     is_pdf_structured = True   
    #     pdf_doc = fitz.open(path)
    #     pdf_field_values = []
    #     pdf_cbs_values = []

    #     for page in pdf_doc:
    #         pdf_field_values.append([field.field_value.strip() for field in page.widgets() if field.field_type == 7])

    #     for page in pdf_doc:
    #         pdf_cbs_values.append([field.field_value.strip() for field in page.widgets() if field.field_type == 2])
    
    # # new structure for fields, cbs, tables etc should be used

    pdf_images = pdf_to_images(pdf)
    print("************ Converted PDF to Images ***************")
    form_refs = get_form_ref('fields1.csv', formtype)
    page_nos = [p-1 for p in set(list(form_refs['PageNumber']))]
    print(f"************ Extracted Reference Fields of the Form {formtype} in pages {page_nos} *************")
    tx_fields = get_taxform_fields(pdf_images, form_refs, page_nos, ocr_reader)
    print('\n\n')
    for tf in tx_fields:
        for f in tf:
            print(f"{f[0]}\t : {f[1]}\n")


    checked_fields = []
    cbs_ref = get_cbs_ref('cbs1.csv', formtype)
    
    for p in page_nos:
        cv2.imwrite('tax_forms/' + formtype + str(p+1) + '.jpeg', pdf_images[p])
        checked_fields.append(get_img_cbs('tax_forms/' + formtype + str(p+1) + 
        '.jpeg',cbs_ref[cbs_ref['PageNumber'] == p+1].reset_index()))
    for cf in checked_fields:
        print(cf.loc[:,['Field', 'PageNumber', 'checked']])

    table = None
    if formtype == 'W-8BEN-E':
       table = get_table_details(pdf_images[-1], ocr_reader)
       for row in table:
           print(row)

    if formtype == 'W-9':
        bxs = get_w9_tin_nums('tax_forms/' + formtype+ str(1) + '.jpeg',ocr_reader)
        print(f"\nSSIN - {bxs[0]}, EIN - {bxs[1]}")

    return [tx_fields,checked_fields, bxs, table]